// Client.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "client.h"


int main(int argc, char* argv[])
{
	if (argc == 1)
	{
		try
		{
			client c("127.0.0.1", 21, "user");
			return c.run();
		}
		catch (...)
		{
			std::cerr << "Error\n";
			return 5;
		}
	}
	if (argc==4)
	{
		try
		{
			client c(argv[1], std::stoi(argv[2]), argv[3]);
			return c.run();
		}
		catch (...)
		{
			std::cerr << "Error\n";
			return 5;
		}
	}
	return 9;
}

int client::run()
{
	if (_preinit)
	{
		_preinit = false;
		try
		{
			session(_set_address, _set_port, _set_name);
			return 0;
		}
		catch (boost::system::system_error ex)
		{
			std::cerr << "Boost error: " << ex.what();
			return -1;
		}
		catch (std::exception ex)
		{
			std::cerr << "Error: " << ex.what();
			return 1;
		}
	}
	return 3;
}

bool client::session(std::string address, unsigned short port, std::string username)
{
	try
	{
		auto endpoint_p = std::make_shared<boost::asio::ip::tcp::endpoint>
			(boost::asio::ip::address::from_string(address), port);
		auto client_id = hash(username);
		auto socket_p = std::make_shared<boost::asio::ip::tcp::socket>(_io_service);
		unsigned long max_wait_ms = _max_wait_ms;
		socket_p->open(boost::asio::ip::tcp::v4());

		message_getter mg(socket_p);
		message_writer mw(socket_p);

		_session_on = true;

		_update_thread = std::thread(&client::update_loop, this, socket_p, endpoint_p, client_id);

		for (bool over = false; !over;)
		{
			std::cout << username << " >: ";
			std::string input;
			std::getline(std::cin, input);
			if (input == "exit")
			{
				over = true;
				continue;
			}

			std::vector <std::string> parsed_input;

			boost::split(parsed_input, input, boost::is_any_of("\t "), boost::token_compress_on);

			if (parsed_input.size() == 0)
				continue;

			std::string command = parsed_input[0];
			std::vector <std::string> args(parsed_input.begin() + 1, parsed_input.end());

			//using mutex in order not to interfere with update loop
			_socket_mutex.lock();
			socket_p->connect(*endpoint_p);
			mw.write(message(command, args, client_id));
			auto m = mg.get_message_for(max_wait_ms);

			socket_p->close();
			_socket_mutex.unlock();

			if (m.client_id() == 0 || m.client_id() == client_id)
				std::cout << m.readable() << std::endl;
			else
				std::cout << "wrong server reply, skipping...";
		}
	}
	catch (boost::system::system_error se)
	{
		_session_on = false;

		std::cerr << "Boost error: " << se.what();
		return false;
	}
	catch (std::exception ex)
	{
		_session_on = false;

		std::cerr << "Error: " << ex.what();
		return false;
	}
	_session_on = false;

	if (_update_thread.joinable())
		_update_thread.join();

	return true;
}

void client::update_loop(std::shared_ptr<boost::asio::ip::tcp::socket> socket_p,
	std::shared_ptr<boost::asio::ip::tcp::endpoint> server_endpoint_p, unsigned long client_id)
{
	std::mutex update_wait_mutex;
	std::unique_lock<std::mutex> update_wait_lock(update_wait_mutex);
	std::condition_variable update_delay_con_v;

	message_writer mw(socket_p);
	message_getter mg(socket_p);
	auto update_message = generate_update_message(client_id);
	while (_session_on)
	{
		_socket_mutex.lock();
		socket_p->connect(*server_endpoint_p);
		mw.write(update_message);
		auto server_m = mg.get_message_for(_max_wait_ms);
		socket_p->close();
		_socket_mutex.unlock();

		std::cout << server_m.readable() << std::endl;

		update_delay_con_v.wait_for(update_wait_lock, std::chrono::milliseconds(_update_period_ms));
	}


}


bool client::valid_set(std::string address, long port)
{
	std::cout << "Checking " << address << ":" << port << " validity\n";
	boost::system::error_code ec;
	boost::asio::ip::address::from_string(address, ec);
	if (ec)
		return false;
	std::cout << "Address valid; checking port\n";
	return port > 0 && port < 65535;
}