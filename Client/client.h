﻿#pragma once

#include <string>


#include <string>
#include <boost/asio.hpp>
#include <iostream>
#include <thread>
#include <boost/algorithm/string.hpp>
#include "message_getter.h"
#include "message_writer.h"


class client
{
public:
	static unsigned long hash(std::string string)
	{
		unsigned long hash_i = 0;

		for (int i = 0; i < string.size(); i++)
		{
			hash_i += string[i] + 42;
			hash_i <<= string[i] % 21;
		}

		return hash_i;
	}
	int run();
	bool valid_set(std::string address, long port);
	client(std::string name="client"):
		client("",0,name)
	{}
	client(std::string address, unsigned short port, std::string username)
	{
		_preinit = valid_set(address, port);
		_set_address = address;
		_set_port = port;
		_set_name = username;
		_io_thread = std::thread(&client::run_io, this);

		//каждые 20000 мс отправлять запрос на обновление
		_update_period_ms =20000;
		_max_wait_ms = 5000;
	}

	bool session(std::string address, unsigned short port, std::string username);
	
	void run_io()
	{
		_work_ptr = std::auto_ptr<boost::asio::io_service::work>(new boost::asio::io_service::work(_io_service));
		_work_ptr.get()->get_io_service().run();
	}

private:
	boost::asio::io_service _io_service;

	std::auto_ptr<boost::asio::io_service::work> _work_ptr;

	std::atomic<std::string> _set_address;
	std::atomic<unsigned short> _set_port;
	std::atomic<std::string> _set_name;

	std::thread _io_thread;
	std::thread _update_thread;
	std::atomic<bool> _session_on;
	bool _preinit;

	message generate_update_message(unsigned long client_id)
	{
		return message("update",client_id);
	}
	void update_loop(std::shared_ptr<boost::asio::ip::tcp::socket> socket_p,
		std::shared_ptr < boost::asio::ip::tcp::endpoint> server_endpoint_p, unsigned long client_id);

	std::condition_variable _update_wait_con_v;

	unsigned long _update_period_ms;
	unsigned long _max_wait_ms;

	std::mutex _socket_mutex;
};
