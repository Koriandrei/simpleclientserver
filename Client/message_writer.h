﻿#pragma once

#include <boost/asio.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>

#include <mutex>

#include "message.h"

class message_writer
{
public:
	message_writer(std::shared_ptr<boost::asio::ip::tcp::socket> socket_p)
	{
		_socket_p = socket_p;
	}
	void write(message message);
	void write(std::string message_string, unsigned long client_id);
private:
	std::shared_ptr<boost::asio::ip::tcp::socket> _socket_p;
};