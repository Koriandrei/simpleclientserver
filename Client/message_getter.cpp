﻿#include "stdafx.h"

#include "message_getter.h"

message message_getter::get_message()
{

	message in_message;

	boost::asio::streambuf buf;

	boost::system::error_code ignored_code;

	try
	{
		boost::asio::read_until(*_socket_p, buf, message::end_seq());
	}
	catch (std::exception)
	{
		return in_message;
	}

	
	boost::archive::binary_iarchive arc(buf);
	arc & in_message;

	return in_message;
}

message message_getter::get_message_for(unsigned long wait_ms)
{
	message in_message;

	boost::asio::streambuf buf;

	try
	{
		boost::system::error_code ignored_code;
		boost::asio::async_read_until(*_socket_p, buf, message::end_seq(),
			boost::bind(&message_getter::message_read_handler, this, boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
	}
	catch (std::exception)
	{
		return in_message;
	}
	try
	{
		std::mutex socket_wait_mutex;
		std::unique_lock<std::mutex> socket_wait_lock(socket_wait_mutex);
		_con_v.wait_for(socket_wait_lock, std::chrono::milliseconds(wait_ms));
	}
	catch (std::exception)
	{

		return in_message;
	}

	if (!_wait_finished)
	{
		boost::system::error_code ignored_code;
		_socket_p->cancel(ignored_code);



		std::mutex handler_return_mutex;
		std::unique_lock<std::mutex> handler_return_lock(handler_return_mutex);

		_con_v.wait_for(handler_return_lock, std::chrono::milliseconds(_failsafe_wait_ms));
		return timed_out_message();
	}

	boost::archive::binary_iarchive arc(buf);
	arc & in_message;

	return in_message;
}

//void message_getter::timer_expired_handler(const boost::system::error_code& ec)
//{
//	boost::system::error_code ignored_code;
//	_socket_p->cancel(ignored_code);
//}

void message_getter::message_read_handler(const boost::system::error_code& ec, size_t bytes_transferred)
{
	//std::cout << "Read handler invoked!\n";


	if (ec)
	{
		//std::cout << "!!! Invoked handler is with errors, but it just goes back!\n";
		_con_v.notify_all();
		return;
	}
	_wait_finished = true;
	//std::cout << "!!! Invoked handler is without errors!\n";
	_con_v.notify_all();
	//	boost::system::error_code ignored_code;
	//	_timer_p->cancel(ignored_code);
}

/*#include "stdafx.h"

#include "message_getter.h"

message message_getter::get_message()
{
	message in_message;

	boost::asio::streambuf buf;

	boost::system::error_code ignored_code;

	boost::asio::read_until(*_socket_p, buf,message::end_seq(),ignored_code);

	try
	{
		boost::archive::binary_iarchive arc(buf);
		arc & in_message;
	}
	catch (std::exception ex)
	{
		std::cout << "message came wrong :c";
	}

	
	return in_message;
}*/