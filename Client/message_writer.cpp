﻿#include "stdafx.h"


#include "message_writer.h"

void message_writer::write(message message)
{
	boost::asio::streambuf buf;
	boost::archive::binary_oarchive arc(buf);

	arc & message;

	std::ostream os(&buf);
	os << message::end_seq();
	os.flush();

	boost::system::error_code ignored_code;
	boost::asio::write(*_socket_p, buf, ignored_code);
}

void message_writer::write(std::string message_string, unsigned long client_id)
{
	write(message::message_display_str(message_string, client_id));
}