﻿#include "stdafx.h"

#include "request.h"

#include <fstream>
#include <sstream>
#include <boost/date_time.hpp>

std::string file_info_snippet(std::string path_string)
{
	boost::filesystem::path path(path_string);
	std::stringstream ss;

	try
	{
		if (boost::filesystem::exists(path))
			if (boost::filesystem::is_regular_file(path))
			{
			auto write_time_t = boost::filesystem::last_write_time(path);
			auto write_posix_time = boost::posix_time::from_time_t(write_time_t);
			auto file_size = boost::filesystem::file_size(path);
			ss << path.string() << std::endl;
			ss << "File size: " << file_size << std::endl <<
				"Last write time: " << write_posix_time;
			return ss.str();
			}
			else
			{
				ss << path_string << ": not a valid file";
				return ss.str();
			}
		else
		{
			ss << path_string << " does not exist";
			return ss.str();
		}
	}
	catch (boost::filesystem::filesystem_error)
	{
		ss << "Error";
		return ss.str();
	}
}

void request::process(client_info &cl_i)
{
	_response = "unknown request";
}

void ls_request::process(client_info &cl_i)
{
	boost::filesystem::path p(_args[0]);
	std::stringstream ss;

	try
	{
		if (exists(p))    // does p actually exist?
			if (is_directory(p))      // is p a directory?
			{
			std::vector<boost::filesystem::path> v;

			copy(boost::filesystem::directory_iterator(p),
				boost::filesystem::directory_iterator(), back_inserter(v));

			sort(v.begin(), v.end());

			for (std::vector<boost::filesystem::path>::const_iterator it(v.begin());
				it != v.end(); ++it)
				ss << "   " << *it << '\n';
			}
			else
				ss << p << " isn't a directory";
		else
			ss << p << " does not exist";
	}

	catch (boost::filesystem::filesystem_error ex)
	{
		ss << ex.what();
	}
	_response = ss.str();
}

void cat_request::process(client_info &cl_i)
{
	std::ifstream fin(_args[0]);

	if (fin.good())
	{
		std::stringstream ss;
		for (; !fin.eof();)
		{
			std::string temp;
			std::getline(fin, temp);
			ss << temp << std::endl;
		}
		_response = ss.str();
		return;
	}
	_response = _args[0] + " is not a valid file";
}

void watch_request::process(client_info &cl_i)
{
	std::string path = _args[0];
	if (boost::filesystem::exists(path))
		if (boost::filesystem::is_regular_file(path))
			if (cl_i.add_path(path))
				_response = "Successfully watched";
			else
				_response = "Already watching";
		else
			_response = path + " is not a valid file";
	else
		_response = path + " does not exist";
}

void unwatch_request::process(client_info &cl_i)
{
	std::string path = _args[0];
	if (cl_i.remove_path(path))
		_response = "Successfully unwatched";
	else
		_response = path + " is not watched";
}

void update_request::process(client_info &cl_i)
{
	std::stringstream ss;
	ss << "Update: " << std::endl;
	for (int i = 0; i < cl_i.watched_paths_count(); i++)
	{
		ss << file_info_snippet(*cl_i.watched_path(i)) << std::endl;
	}

	_response = ss.str();
}