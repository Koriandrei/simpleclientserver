﻿#pragma once

#include <string>
#include <vector>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>

class client_info;

#include "request.h"

class client_info
{
public:
	template <class serialization_archive>
	void serialize(serialization_archive &ar, const unsigned int version)
	{
		ar & _hash;
		ar & _watched_paths;
	}

	client_info(unsigned long hash)
	{
		_hash = hash;
	}

	int watched_paths_count()
	{
		return _watched_paths.size();
	}

	bool add_path(std::string path)
	{
		for (int i = 0; i < _watched_paths.size(); i++)
			if (_watched_paths[i] == path)
				return false;
		_watched_paths.push_back(path);
		return true;
	}
	bool remove_path(std::string path)
	{
		for (int i = 0; i < _watched_paths.size();i++)
			if (_watched_paths[i]==path)
			{
			_watched_paths.erase(_watched_paths.begin() + i);
			return true;
			}
		return false;
	}
	
	//returns a const pointer (&s) to a string or nullptr, if index is out of scope
	const std::string* watched_path(int i)
	{
		if (i>=0 && i<_watched_paths.size())
			return &_watched_paths[i];
		return nullptr;
	}
	friend class request;
	unsigned long hash(){ return _hash; }

private:
	client_info(const client_info &other)
	{
		throw std::exception("client_info is non-copyable!");
	}
	std::vector<std::string> _watched_paths;
	unsigned long _hash;
};
