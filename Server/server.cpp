// Server.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "server.h"

int main(int argc, char* argv[])
{
	if (argc == 1)
	{
		try
		{
			server s;
			s.start();
		}
		catch (boost::system::system_error se)
		{
			std::cerr << "Boost error: " << se.what();
			return -1;
		}
		catch (std::exception ex)
		{
			std::cerr << "Error: " << ex.what();
			return 1;
		}
		return 0;
	}
	return 3;
}

void server::start()
{
	_log_provider.log("STR: Beginning starting sequence.");

	size_t next_handler = 0;
	_running = true;

	_io_thread = std::thread(&server::handle_io_service, this);

	_active_clients = std::make_shared<active_clients>(_fetch_check_ms);

	for (; next_handler < _handlers_count; next_handler++)
	{
		_handlers_datas.push_back(std::make_shared< connection_handler_data>(next_handler, _active_clients));
		_handler_threads.push_back(
			std::thread(&server::handle_connections, this, _handlers_datas[next_handler])
			);
		_log_provider.log("STR: Handler thread ", next_handler, " initialized.");
	}

	_log_provider.log("STR: All handler threads initialized.");

	boost::asio::ip::tcp::endpoint v4_port_endpoint(boost::asio::ip::tcp::v4(), _port);

	_acceptor_p = std::make_shared< boost::asio::ip::tcp::acceptor>(_io_service, v4_port_endpoint);

	_log_provider.log("STR: Acceptor on port ", _acceptor_p->local_endpoint().port(), " initialized.");

	_acceptor_thread = std::thread(&server::accept_connections, this);

	_log_provider.log("STR: Acceptor thread started.");

	if (_preinit)
	{
		_log_provider.log("STR: Server was preinitialized. Beginning keyboard input handling.");
		_preinit = false;
		handle_keyb_input();

		return;
	}

	_log_provider.log("STR: Starting sequence finished.");
}

void server::stop()
{
	_log_provider.log("STP: Beginning stopping sequence.");

	_running = false;

	_work_ptr.reset();

	_log_provider.log("STP: Notification about server stopping sent.");

	_acceptor_thread.join();

	_log_provider.log("STP: Acceptor thread finished.");

	_acceptor_p->cancel();
	_acceptor_p->close();

	_active_clients->set_server_running(false);

	for (int i = 0; i < _handlers_count; i++)
	{
		//_handlers_datas[i]->set_server_running(false);
		if (_handler_threads[i].joinable())
			_handler_threads[i].join();

		_log_provider.log("STP: Handler thread ", i, " finished.");
	}

	_handler_threads.clear();
	_log_provider.log("STP: Cleared handler threads.");

	_handlers_datas.clear();
	_log_provider.log("STP: Created handler threads' data.");

	_log_provider.log("STP: Stopping sequence finished.");
}



void server::accept_connections()
{
	std::stringstream log_stream;
	std::mutex accept_mutex;
	std::unique_lock<std::mutex> accept_lock(accept_mutex);

	for (; _running;)
	{
		std::shared_ptr<boost::asio::ip::tcp::socket> socket_p =
			std::make_shared< boost::asio::ip::tcp::socket>(_io_service);
		_connection_accepted = false;
		_log_provider.log("ACC: New accept socket created. Waiting for connection...");
		for (; _running && !_connection_accepted;)
		{
			_acceptor_p->async_accept(*socket_p,
				boost::bind(&server::client_accepted_handler, this, boost::asio::placeholders::error));
			_accept_con_v.wait_for(accept_lock, std::chrono::milliseconds(_acceptor_check_ms));
			if (!_running)
			{
				boost::system::error_code ignored_code;
				_acceptor_p->cancel(ignored_code);
			}
		}

		_log_provider.log("ACC: Connection established. Socket filled. Rerouting task to a handler thread...");

		//int actual_handler;
		//something like ring buffer for tasks
		//_handlers_datas[actual_handler = _next_handler]->AddSocket(socket_p);
		_active_clients->add_socket(socket_p);
		//_next_handler = (_next_handler + 1) % _handlers_count;

		_log_provider.log("ACC: Socket rerouted to handler threads.");
	}
}

std::shared_ptr<client_info> server::client_by_id(unsigned long client_id)
{
	_clients_mutex.lock();
	for (int i = 0; i < _clients.size(); i++)
		if (_clients[i]->hash() == client_id)
		{
		_clients_mutex.unlock();
		return _clients[i];
		}
	_clients_mutex.unlock();
	return nullptr;
}


std::shared_ptr<client_info> server::create_client(unsigned long client_id)
{
	_clients_mutex.lock();
	std::shared_ptr<client_info> ci;
	if ((ci = client_by_id(client_id)) != nullptr)
	{
		_clients_mutex.unlock();
		return ci;
	}
	ci = std::make_shared< client_info>(client_id);

	_clients.push_back(ci);
	_clients_mutex.unlock();
	return ci;
}

void server::handle_connections(std::shared_ptr<connection_handler_data> data)
{
	std::string handler_mark = "HND#" + std::to_string(data->get_number()) + ": ";
	_log_provider.log(handler_mark, "Started.");

	for (; _running;)
	{
		_log_provider.log(handler_mark, "Fetching socket.");

		auto socket_p = data->fetch_socket();

		if (socket_p == nullptr)
		{
			_log_provider.log(handler_mark, "No socket fetched.");
			continue;
		}

		_log_provider.log(handler_mark, "Socket fetched.");

		message_getter getter(socket_p);
		message_writer writer(socket_p);

		_log_provider.log(handler_mark, "Getting client message.");

		auto client_message = getter.get_message_for(_max_wait_ms);

		_log_provider.log(handler_mark + "Client message: \"", client_message.readable(), "\"");

		auto verdict = analyze(client_message);
		if (verdict.invalid())
		{
			_log_provider.log(handler_mark, "Verdict: message invalid.");

			boost::system::error_code ignored_code;
			writer.write(message_invalid(verdict.client_id()));
			socket_p->close(ignored_code);
			continue;
		}

		if (!verdict.client_exists())
			if (new_clients_allowed())
				verdict.assign_client(create_client(verdict.client_id()));
			else
			{
				boost::system::error_code ignored_code;
				writer.write(message_new_clients_not_allowed(verdict.client_id()));
				socket_p->close(ignored_code);

				_log_provider.log(handler_mark + "Client with ID ", verdict.client_id(),
					"does not exist. Request rejected.");

				continue;
			}

		_log_provider.log(handler_mark, "Processing request...");

		auto r = verdict.request();
		r->process(*verdict.client());

		_log_provider.log(handler_mark + "Done. Request response: \"", r->response(), "\"");

		writer.write(r->response(), verdict.client_id());

		_log_provider.log(handler_mark, "Response sent. Closing socket.");

		boost::system::error_code ignored_code;
		socket_p->close(ignored_code);
	}

	//handling is over, so destroying data
	data.reset();

	_log_provider.log(handler_mark + "Out of the loop. Finishing.");
}
verdict server::analyze(message client_message)
{
	unsigned long client_id = client_message.client_id();
	switch (client_message.arg_count())
	{
	case 0:
		if (client_message.command() == "update")
		{
			return verdict::v_update(client_id, client_by_id(client_id));
		}
		return verdict::v_incorrect();
	case 1:
		if (client_message.command() == "ls")
		{
			return verdict::v_ls(client_id, client_by_id(client_id), client_message.arg(0));
		}
		if (client_message.command() == "cat")
		{
			return verdict::v_cat(client_id, client_by_id(client_id), client_message.arg(0));
		}
		if (client_message.command() == "watch")
		{
			return verdict::v_watch(client_id, client_by_id(client_id), client_message.arg(0));
		}
		if (client_message.command() == "unwatch")
		{
			return verdict::v_unwatch(client_id, client_by_id(client_id), client_message.arg(0));
		}
		return verdict::v_incorrect();
	default:
		return verdict::v_incorrect();
	}
	return verdict::v_incorrect();
}


void server::handle_keyb_input()
{
	for (bool exit = false; !exit;)
	{
		std::string keyb_string;
		getline(std::cin, keyb_string);

		std::vector<std::string> input;

		boost::split(input, keyb_string, boost::is_any_of("\t "), boost::token_compress_on);

		switch (input.size())
		{
		case 0:
			continue;
			break;
		case 1:
			if (input[0] == "exit")
			{
				exit = true;
				if (_running)
					stop();
				continue;
			}
			if (input[0] == "stop")
			{
				if (_running)
					stop();
				std::cout << "Server stopped." << std::endl;
				continue;
			}
			if (input[0] == "start")
			{
				if (!_running)
					start();
				continue;
			}
			if (input[0] == "clients")
			{
				std::stringstream clients_information;
				for (int i = 0; i < _clients.size(); i++)
				{
					clients_information << "Client #" << _clients[i]->hash() << std::endl;
					clients_information << "Watches:" << std::endl;
					for (int j = 0; j < _clients[i]->watched_paths_count(); j++)
						clients_information << *_clients[i]->watched_path(j) << std::endl;
					clients_information << std::endl;
				}
				printf_s(clients_information.str().c_str());
				continue;
			}
			break;
		case 2:
			if (input[0] == "port")
			{
				if (input[1] == "running")
				{
					try
					{
						std::cout << "Running port is " << _acceptor_p->local_endpoint().port() << "."
							<< std::endl;
					}
					catch (boost::system::system_error)
					{
						std::cout << "Running port cannot be fetched." << std::endl;
					}
					continue;
				}
				if (input[1] == "set")
				{
					std::cout << _port << std::endl;
					continue;
				}
				long port_cand;
				try
				{
					port_cand = std::stol(input[1]);
				}
				catch (std::exception)
				{
					std::cout << keyb_in_msg_wrong_port() << std::endl;
					continue;
				}
				if (port_cand <= 0 && port_cand > 65535)
				{
					std::cout << keyb_in_msg_wrong_port() << std::endl;
					continue;
				}
				_port = port_cand;
				std::cout << keyb_in_msg_port_set() << std::endl;
				continue;
			}
			if (input[0] == "newclients")
			{
				if (input[1] == "on")
				{
					_new_clients_allowed = true;
					std::cout << keyb_in_msg_new_clients_to_on() << std::endl;
					continue;
				}
				if (input[1] == "off")
				{
					_new_clients_allowed = false;
					std::cout << keyb_in_msg_new_clients_to_off() << std::endl;
					continue;
				}
				std::cout << keyb_in_msg_problem_parsing();
			}
			break;
		case 3:
			if (input[0] == "handlers")
			{
				if (input[1] == "count")
				{
					if (input[2] == "active")
					{
						std::cout << "There are " << _handler_threads.size() << " active handlers." <<
							std::endl;
						continue;
					}
					if (input[2] == "set")
					{
						std::cout << "Handlers count is set to " << _handlers_count << "." << std::endl;
					}

				}
			}
			if (input[0] == "save")
			{
				if (input[1] == "clients")
					if (save_clients(input[2]))
						std::cout << keyb_in_msg_clients_saved() << std::endl;
					else
						std::cout << keyb_in_msg_problem_saving() << std::endl;
				if (input[1] == "settings")
					if (save_settings(input[2]))
						std::cout << keyb_in_msg_settings_saved() << std::endl;
					else
						std::cout << keyb_in_msg_problem_saving() << std::endl;
				continue;
			}
			if (input[0] == "load")
			{
				if (input[1] == "clients")
					if (load_clients(input[2]))
						std::cout << keyb_in_msg_clients_loaded() << std::endl;
					else
						std::cout << keyb_in_msg_problem_loading() << std::endl;
				if (input[1] == "settings")
					if (load_settings(input[2]))
						std::cout << keyb_in_msg_settings_loaded() << std::endl;
					else
						std::cout << keyb_in_msg_problem_loading() << std::endl;
				continue;
			}
		default:
			break;
		}

		std::cout << keyb_in_msg_problem_parsing() << std::endl;
	}
}