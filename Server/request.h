﻿#pragma once

#include <string>
#include <boost/filesystem.hpp>

class request;

#include "client_info.h"

//request - абстрактный "запрос"
//наследники request - конкретные команды:
//cat_request, ls_request...
//
class request
{
public:
	request(std::string arg)
	{
		_response = ("request not processed");
		_args.push_back(arg);
	}
	request(std::vector<std::string> args)
	{
		_response = std::string("request not processed");
		_args = std::vector<std::string>(args);
	}

	virtual void process(client_info &cl_i);
	std::string response(){ return _response; }
	virtual ~request(){}

protected:
	std::string _response;
	std::vector<std::string> _args;

private:
	request(const request &other)
	{
		throw std::exception("request is uncopyable!");
	}
};

class ls_request:
	public request
{
public:
	ls_request(std::string path) :
		request(path)
	{}
	void process(client_info &cl_i) override;
};

class cat_request:
	public request

{
public:
	cat_request(std::string path) :
		request(path)
	{}
	void process(client_info &cl_i) override;
	
};

class watch_request:
	public request
{
public:
	watch_request(std::string path) :
		request(path)
	{}
	void process(client_info &cl_i) override;
	
};

class unwatch_request:
	public request
{
public:
	unwatch_request(std::string path) :
		request(path)
	{}
	void process(client_info &cl_i) override;
	
};

class update_request:
	public request
{
public:
	update_request():
		request(std::vector<std::string>())
	{}
	void process(client_info &cl_i) override;
};