﻿#pragma once

#include <string>
#include <vector>
#include <sstream>
class message
{
public:
	message()
	{
		_client_id = 0;
	}


	message(std::string command,unsigned long client_id)
	{
		_command = command;
		_client_id = client_id;
	}
	message(std::string command, std::string arg, unsigned long client_id)
	{
		_command = command;
		_args.push_back(arg);
		_client_id = client_id;
	}
	message(std::string command, std::string arg1, std::string arg2,  unsigned long client_id)
	{
		_command = command;
		_args.push_back(arg1);
		_args.push_back(arg2);
		_client_id = client_id;
	}
	message(std::string command, std::vector<std::string> args, unsigned long client_id)
	{
		_command = command;
		_args = std::vector<std::string>(args);
		_client_id = client_id;
	}

	std::string command(){ return _command; }

	static message message_display_str(std::string str,unsigned long client_id)
	{
		return message("display",str,client_id);
	}

	std::string readable()
	{
		std::stringstream ss;
		ss << _command << " ";
		{
			for (int i = 0; i < _args.size(); i++)
				ss << _args[i] << " ";
		}

		ss << _client_id;
		 
		return ss.str();
	}

	unsigned long client_id()
	{
		return _client_id;
	}

	static std::string end_seq()
	{
		return "111111";
	};

	

	std::string arg(size_t arg_n)
	{ 
		if (arg_n<_args.size())
			return _args[arg_n]; 
		return "requested index out of bounds";
	}
	size_t arg_count(){ return _args.size(); }
	template <class serialization_archive>
	void serialize(serialization_archive &ar, const unsigned int version)
	{
		ar & _command;
		ar & _args;
		ar & _client_id;
	}
private:
	std::string _command;
	std::vector<std::string> _args;
	unsigned long _client_id;
};
