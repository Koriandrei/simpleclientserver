#pragma once

#include <boost/asio.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>


#include <boost/algorithm/string.hpp>

#include <iostream>
#include <thread>
#include <atomic>
#include <vector>
#include <queue>
#include <condition_variable>

#include "client_info.h"

#include "message.h"

#include "message_getter.h"
#include "message_writer.h"

#include "request.h"

#include "verdict.h"

#include "log_provider.h"

class server
{
	class connection_handler_data;
	class active_clients;
	class connection_handler_data
	{
	public:
		connection_handler_data(int number, std::shared_ptr<active_clients> active_clients_p)
		{
			_number = number;
			_active_clients_p = active_clients_p;
		}

		std::shared_ptr<boost::asio::ip::tcp::socket> fetch_socket()
		{
			return _active_clients_p->fetch_socket();
		}

		int get_number()
		{
			return _number;
		}

	private:
		connection_handler_data(connection_handler_data& other)
		{
			throw std::exception("trying to copy uncopyable object!");
		}

		int _number;
		std::shared_ptr<active_clients> _active_clients_p;
	};
	class active_clients
	{
	public:
		active_clients(unsigned long fetch_check_ms)
		{
			_server_running = true;
			_fetch_check_ms = fetch_check_ms;
		}
		void set_server_running(bool running)
		{
			_server_running = running;
		}
		void add_socket(std::shared_ptr<boost::asio::ip::tcp::socket> socket)
		{
			_access_mutex.lock();
			_sockets.push(socket);
			_access_mutex.unlock();
			_fetch_con_v.notify_one();
		}

		std::shared_ptr<boost::asio::ip::tcp::socket> fetch_socket()
		{
			std::mutex fetch_mutex;
			std::unique_lock<std::mutex> fetch_lock(fetch_mutex);
			while (_sockets.empty() && _server_running)
				_fetch_con_v.wait_for(fetch_lock, std::chrono::milliseconds(_fetch_check_ms));
			if (!_server_running)
				return nullptr;
			_access_mutex.lock();
			auto socket = _sockets.front();
			_sockets.pop();
			_access_mutex.unlock();
			return socket;
		}
		bool is_running()
		{
			return _server_running;
		}
	private:
		std::mutex _access_mutex;
		std::queue<std::shared_ptr<boost::asio::ip::tcp::socket> > _sockets;
		std::condition_variable _fetch_con_v;
		unsigned long _fetch_check_ms;
		std::atomic<bool> _server_running;
	};

public:
	enum logger_type
	{
		no_logging,
		console_logging,
		file_logging
	};

	server()
	{
		_preinit = true;
		_port = 21;
		_handlers_count = 2;
		_new_clients_allowed = true;
		_max_wait_ms = 5000;
		_fetch_check_ms = 5000;
		_acceptor_check_ms = 10000;

		_log_provider.set_logger(new console_logger());
		_running = false;

	}

	server(unsigned short port, size_t handlers_count, bool new_clients_allowed,
		unsigned long fetch_check_ms, unsigned long max_wait_ms, unsigned long acceptor_check_ms,
		logger_type logger_type, std::string logfile_path = "")
	{
		_preinit = true;
		_port = port;
		_handlers_count = handlers_count;
		_new_clients_allowed = new_clients_allowed;
		_max_wait_ms = max_wait_ms;
		_fetch_check_ms = fetch_check_ms;
		_acceptor_check_ms = acceptor_check_ms;
		switch (logger_type)
		{
		case no_logging: break;
		case console_logging:
			_log_provider.set_logger(new console_logger());
			break;
		case file_logging:
			try
			{
				_log_provider.set_logger(new file_logger(logfile_path));
			}
			catch (std::exception ex)
			{
				//if ()
				std::cerr << logfile_path << " is not a valid file path for a logfile";
				throw;
			}
			break;
		default:
			std::cerr << "Invalid logger type";
			throw std::exception("Invalid logger type");
			break;
		}
	}


	void start();
	void stop();
	void handle_keyb_input();

private:
	std::atomic<bool> _running;

	unsigned short _port;

	boost::asio::io_service _io_service;
	std::auto_ptr< boost::asio::io_service::work> _work_ptr;

	//acceptor stuff
	std::thread _acceptor_thread;
	std::shared_ptr<boost::asio::ip::tcp::acceptor> _acceptor_p;

	std::condition_variable _accept_con_v;
	std::atomic<bool> _connection_accepted;

	void accept_connections();
	void client_accepted_handler(const boost::system::error_code& ec)
	{
		if (ec)
			return;
		_connection_accepted = true;
		_accept_con_v.notify_all();
	}

	//clients stuff
	std::vector<std::shared_ptr<client_info> > _clients;

	std::recursive_mutex _clients_mutex;
	bool new_clients_allowed()
	{
		return _new_clients_allowed;
	}
	std::atomic_bool _new_clients_allowed;

	std::shared_ptr<client_info> create_client(unsigned long client_id);
	std::shared_ptr<client_info> client_by_id(unsigned long client_id);

	//connection handling stuff
	size_t _handlers_count;

	std::vector<std::thread> _handler_threads;
	std::vector<std::shared_ptr<connection_handler_data>> _handlers_datas;
	std::shared_ptr<active_clients> _active_clients;
	void handle_connections(std::shared_ptr<connection_handler_data> data);
	verdict analyze(message client_message);

	//io_service handling stuff
	std::thread _io_thread;
	void handle_io_service()
	{
		_log_provider.log("IOT: Starting io_service async run.");

		_work_ptr = std::auto_ptr<boost::asio::io_service::work>(new boost::asio::io_service::work(_io_service));
		_work_ptr.get()->get_io_service().run();
		_log_provider.log("IOT: io_service async run finished.");
	}

	//delays
	unsigned long _fetch_check_ms;
	unsigned long _max_wait_ms;
	unsigned long _acceptor_check_ms;





	bool _new_clients_allowed_load;

	bool save_clients(std::string file_path)
	{
		std::ofstream fout(file_path);
		if (!fout.good())
			return false;
		_clients_mutex.lock();
		size_t cli_count = _clients.size();
		fout << cli_count << '\n';
		for (int i = 0; i < cli_count; i++)
		{
			size_t paths_c = _clients[i]->watched_paths_count();
			fout << _clients[i]->hash() << '\t' << paths_c << '\n';
			for (int j = 0; j < paths_c; j++)
				fout << *_clients[i]->watched_path(j) << '\n';
		}
		_clients_mutex.unlock();
		fout.close();
		return true;
	}
	bool save_settings(std::string file_path)
	{
		std::ofstream fout(file_path);
		if (!fout.good())
			return false;

		fout << _port << '\n';
		fout << _handlers_count << '\n';
		fout << _acceptor_check_ms << '\n';
		fout << _fetch_check_ms << '\n';
		fout << _max_wait_ms << '\n';
		fout << static_cast<bool>(new_clients_allowed) << '\n';

		fout.close();
		return true;
	}
	bool load_clients(std::string file_path)
	{
		if (_running)
			return false;

		std::ifstream fin(file_path);
		if (!fin.good())
			return false;

		_clients_mutex.lock();
		
		
		size_t cli_count;
		fin >> cli_count;
		for (int i = 0; i < cli_count; i++)
		{
			unsigned long client_id;
			size_t paths_c;
			fin >> client_id >> paths_c;
			_clients.push_back(std::make_shared<client_info>(client_id));
			for (int j = 0; j < paths_c; j++)
			{
				std::string watched_path;
				getline(fin, watched_path);
				_clients[_clients.size() - 1]->add_path(watched_path);
			}
		}

		_clients_mutex.unlock();
		/*
		boost::archive::text_iarchive ar(fin);
		std::vector<std::shared_ptr<client_info> > newclients;
		boost::serialization::load(ar, newclients, 0);
		_clients.emplace_back(newclients);*/
		return true;
	}
	bool load_settings(std::string file_path)
	{
		if (_running)
			return false;
		std::ifstream fin(file_path);
		if (!fin.good())
			return false;

		fin >> _port;
		fin >> _handlers_count;
		fin >> _acceptor_check_ms;
		fin >> _fetch_check_ms;
		fin >> _max_wait_ms;
		bool new_clients_allowed_load;
		fin >> new_clients_allowed_load;

		_new_clients_allowed = new_clients_allowed_load;
		return true;
	}


	//logger
	log_provider _log_provider;

	//server is non-copyable; making copy constructor private
	server(const server &other)
	{
		throw std::exception("server is uncopyable!");
	}

	//messages
	message message_invalid(unsigned long client_id)
	{
		return message::message_display_str("Invalid message.", client_id);
	}
	message message_new_clients_not_allowed(unsigned long client_id)
	{
		return message::message_display_str("New clients are not allowed.", client_id);
	}

	std::string keyb_in_msg_wrong_port()
	{
		return "Port incorrect.";
	}
	std::string keyb_in_msg_port_set()
	{
		return "Port set.";
	}
	std::string keyb_in_msg_problem_parsing()
	{
		return "Problem parsing command.";
	}
	std::string keyb_in_msg_clients_saved()
	{
		return "Clients saved successfully.";
	}
	std::string keyb_in_msg_settings_saved()
	{
		return "Settings saved successfully.";
	}
	std::string keyb_in_msg_problem_saving()
	{
		return "Problem when saving.";
	}
	std::string keyb_in_msg_clients_loaded()
	{
		return "Clients loaded successfully.";
	}
	std::string keyb_in_msg_settings_loaded()
	{
		return "Settings loaded successfully.";
	}
	std::string keyb_in_msg_problem_loading()
	{
		return "Problem when loading.";
	}
	std::string keyb_in_msg_new_clients_to_on()
	{
		return "New clients are now allowed.";
	}
	std::string keyb_in_msg_new_clients_to_off()
	{
		return "New clients are now not allowed.";
	}

	bool _preinit;
};
