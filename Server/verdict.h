﻿#pragma once

#include "client_info.h"
#include "request.h"

class verdict
{
public:
	~verdict()
	{
	}
	bool invalid()
	{
		return _invalid;
	}
	bool client_exists()
	{
		return _client_p != nullptr;
	}
	void assign_client(std::shared_ptr<client_info> client)
	{
		_client_p = client;
	}
	unsigned long client_id()
	{
		return _client_id;
	}
	std::shared_ptr<request> request(){ return _request; }
	std::shared_ptr<client_info> client(){ return _client_p; }

	//verdict generation methods
	static verdict v_ls(unsigned long client_id, std::shared_ptr<client_info> client_by_id, std::string file_path)
	{
		verdict v;
		v._invalid = false;
		v._client_id = client_id;
		v._client_p = client_by_id;
		v._request = std::make_shared<  ls_request>(file_path);
		return v;
	}
	static verdict v_cat(unsigned long client_id, std::shared_ptr<client_info> client_by_id, std::string file_path)
	{
		verdict v;
		v._invalid = false;
		v._client_id = client_id;
		v._client_p = client_by_id;
		v._request = std::make_shared< cat_request>(file_path);
		return v;
	}
	static verdict v_watch(unsigned long client_id, std::shared_ptr<client_info> client_by_id, std::string file_path)
	{
		verdict v;
		v._invalid = false;
		v._client_id = client_id;
		v._client_p = client_by_id;
		v._request = std::make_shared<watch_request>(file_path);
		return v;
	}
	static verdict v_unwatch(unsigned long client_id, std::shared_ptr<client_info>  client_by_id, std::string file_path)
	{
		verdict v;
		v._invalid = false;
		v._client_id = client_id;
		v._client_p = client_by_id;
		v._request = std::make_shared< unwatch_request>(file_path);
		return v;
	}

	static verdict v_update(unsigned long client_id, std::shared_ptr<client_info> client)
	{
		verdict v;
		v._invalid = false;
		v._client_id = client_id;
		v._client_p = client;
		v._request = std::make_shared< update_request>();
		return v;
	}

	static verdict v_incorrect()
	{
		verdict v;
		v._client_id = 0;
		v._client_p = nullptr;
		v._request = nullptr;
		v._invalid = true;
		return v;
	}
private:
	verdict()
	{}

	bool _invalid;
	std::shared_ptr<client_info> _client_p;
	unsigned long _client_id;
	std::shared_ptr<class request> _request;
};
