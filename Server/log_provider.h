﻿#pragma once

#include <atomic>

#include <string>
#include <fstream>
#include <iostream>
#include <mutex>

class log_provider;

class logger abstract
{
public:
	logger()
	{}
	virtual ~logger()
	{
	}

	virtual void log(std::string data) = 0;
private:
	logger(logger &other)
	{
		throw std::exception("logger is uncopyable!");
	}
};

class null_logger :
	public logger
{
public:
	void log(std::string data) override
	{
		//doing nothing
	}
};

class console_logger :
	public logger
{
public:
	void log(std::string data) override
	{
		data.append("\r\n");	//??
		printf_s(data.c_str());
	}
};

class file_logger :
	public logger
{
public:
	file_logger(std::string file_path)
	{
		_fout = std::ofstream(file_path);
	}
	~file_logger() override
	{
		_fout.close();
	}
	void log(std::string data) override
	{
		_fout << data << std::endl;
	}

private:
	std::ofstream _fout;
};

class log_provider
{
public:
	log_provider()
	{
		_logger_impl = new null_logger();
	}
	void log(std::string data)
	{
		_logging_mutex.lock();
		_logger_impl->log(data);
		_logging_mutex.unlock();
	}
	void log(std::string data1,int value,std::string data2)
	{
		std::stringstream ss;
		ss << data1 << value << data2;
		log(ss.str());
	}
	void log(std::string data1, std::string data2)
	{
		std::stringstream ss;
		ss << data1 << data2;
		log(ss.str());
	}
	void log(std::string data1, std::string data2,std::string data3)
	{
		std::stringstream ss;
		ss << data1 << data2 << data3;
		log(ss.str());
	}
	void set_logger(logger* logger)
	{
		_logging_mutex.lock();
		if (_logger_impl != nullptr)
		{
			delete _logger_impl;
			_logger_impl = nullptr;
		}
		_logger_impl = logger;
		_logging_mutex.unlock();
	}
private:
	log_provider(log_provider &other)
	{
		throw std::exception("log_provider is uncopyable!");
	}
	logger* _logger_impl;
	std::mutex _logging_mutex;
};

