﻿#pragma once

#include <boost/asio.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>

#include <boost/bind.hpp>
#include <condition_variable>


#include <atomic>
#include <thread>

#include "message.h"

class message_getter
{
public:
	message_getter(std::shared_ptr<boost::asio::ip::tcp::socket> socket_p)
	{
		_socket_p = socket_p;
	}

	message get_message();
	message get_message_for(unsigned long wait_ms);
private:
	std::atomic_bool _wait_finished;
	std::shared_ptr<boost::asio::ip::tcp::socket> _socket_p;
	std::condition_variable _con_v;
	std::mutex _mutex;
	const unsigned long _failsafe_wait_ms=1000;
	void message_read_handler(const boost::system::error_code &ec, size_t bytes_transferred);

	message_getter(const message_getter &other)
	{
		throw std::exception("message_getter is uncopyable!");
	}
};
